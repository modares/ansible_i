https://mike42.me/blog/2019-01-the-top-100-ansible-modules

- user
- uri
- file
- apt, yum, package
- debug
- ping
- copy
- fetch
- service
- lineinfile
- archive & unarchive
- command & shell
- assert
- stat
- get_url
- fail
- pip