# iteration and vars from a file

```yml
- hosts: all
  vars:
    lll:
      - "as"
      - "asc"
      - "axcdf"
  vars_files:
    - vv.yml
  tasks:
    - name: "output stat task"
      debug:
        msg: "{{item}}"
      with_items: "{{ lll }}"
```

# tags
```yml
- hosts: all
  tasks:
    - name: "output stat task"
      debug:
        msg: "hi"
      tags: install, jighad
```
```bash
ansible-playbook -t install,jighad
ansible-playbook --list-tags
ansible-playbook --skip-tags
```

# start after a task
Note: this will forget the vars if there are vars created in some tasks
```bash
ansible-playbook main.yml --start-at-task "change content of file"
```

# debugging

```bash
ansible-playbook main.yml --step
```
```yml
- hosts: all
  tasks:
    - name: "output stat task"
      apt:
        name: "hi"
      debugger: always
```

# conditioning

```bash
ansible-playbook main.yml --step
```
```yml
- hosts: all
  tasks:
    - name: "output stat task"
      apt:
        name: "hi"

      debugger: always
      # starts debugging only on that step:
      # option that it shows:
        ok: [3.235.124.70]
        [3.235.124.70] TASK: check if file exists (debug)> h

        Documented commands (type help <topic>):
        ========================================
        EOF  c  continue  h  help  p  pprint  q  quit  r  redo  u  update_task

      when:
        - (tt == True) or (False)
        - True
        # Having it as a list will tread them as and conditions

```

# creating a file
For creating a file and adding content to it you should use the copy module

# Multiline strings?
content: |
      Good Morning!
      Awesome sunshine today.