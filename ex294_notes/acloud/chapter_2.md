# std*
<p>
>, >><br>
<, << (?)<br>
2>, 2>><br>
|<br>
</p>

# grep
<p>
grep <pattern> <file>
</p>


# chown
<p>
chown <user>:<group> file
</p>

# chmod
<p>
chmod ugo(+ or -)rwx file
</p>

# tar
<p>
tar -cvzf <tar file name> file_1 file_2 ...
tar -xvzf <tar file name>
</p>

# setuid/setgid/ sticky bit???

# journalcrl -xe
for showing log of services

# systemctl
systemctl enable/disable <service name> for starting it in on boot

# kill -9 vs -15
also, use kill to kill the parent process. (-9 forces, -15 gracefully closes)

# scp and sftp commands

# size:
df -h
lsblk
blkid
fdisk -l
fdisk <disk name> -> a whole tool that takes time to learn? kolle filme e acloud ro bebin, 10 min e (Configure Local Storage)



