from pudb import set_trace as st
cost = [1,100,1,1,1,100,1,1,100,1]

cache = {0: cost[0], 1: min(cost[1], cost[0])}
def helper(cost, n):
    st()
    if n in cache.keys():
        return cache[n]
    else:
        val = min(helper(cost, n-2), helper(cost, n-1))
        cache[n] = val
        return val
print(cache)

print(helper(cost.extend([0]), len(cost)))