import requests
from ipdb import set_trace as st

base_url = "http://localhost:8043/"
inventory_method = "api/v2/inventories/"


def main():
    url = f"{base_url}{inventory_method}"
    res = requests.get(url)
    st()
    # https://localhost:8043/api/v2/inventories/

main()