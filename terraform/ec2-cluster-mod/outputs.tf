output "public_ips" {
  value = {
    "${var.cluster_prefix}" : ([
      for ec2 in aws_instance.aws_ec2_cluster : ec2.public_ip
    ])
  }

}