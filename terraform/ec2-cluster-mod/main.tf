locals {
  key_pair_name  = "${var.cluster_prefix}-key"
  ssh_public_key = var.ssh_public_key
  role_tag       = var.role_tag
}

resource "aws_instance" "aws_ec2_cluster" {
  count = var.node_count

  # ami                         = "ami-026b57f3c383c2eec"
  # ami                         = "ami-08c40ec9ead489470" # Ubutnu
  # ami                         = "ami-08e637cea2f053dfa" # Redhat 9
  # ami                         = "ami-045393c081cabeb1f" # Redhat 7.9
  ami                         = "ami-0aa7d40eeae50c9a9" # Amazon
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  key_name                    = local.key_pair_name
  vpc_security_group_ids      = [aws_security_group.allow_all.id]

  tags = {
    Name = "${var.cluster_prefix}_${count.index}"
    Role = local.role_tag
  }

  # lifecycle {
  #   prevent_destroy = can(var.keep_instance_ids)
  # }

  # dynamic "prevent_destroy" {
  #   for_each = var.keep_instance_ids
  #   content {
  #     instance_id = prevent_destroy.value
  #   }
  # }

}

# PUBLIC_KEY
resource "aws_key_pair" "ec2_cluster_key" {
  key_name = local.key_pair_name
  # public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCywzlyebnuQ8M6eGSkJfQeU+bX+QyykUH9ku69bhvnIU4+eKvVq/SOWl8x0xhe961ge4Z0XbRyWSFvM5TdQr/slRaiEaTubar4Wb7B5HljSsefJrp3PBiuwaVq5SOWUwnvqzzoS5uBKr13hdbpD9EQOGQ8FcVtxFFDbVFhPg5klg7IebIeGoltjcSFIsYv+OcNL1IwLAv1W6sBRdh3F4eKft57dYtSD3USTjQzH8Q2fTZ2n1oOz1i6Pk7reNyvb5R5xXiTtjtOhd7pdELtP9MN1pnZmNSbdyaL/lBFBhI5L7bfpFB54T28bXaHlKqFOM3zptpEhH/DyoVJbRNhAVpLpLg0HVKtyEI+OC52wWd/8Div5q1xFVkWcORUot+4VVc8u2gx/vELYXM1xerIcDqUayUyLxn681+/S3n7l+HylXiWwhwsFlUXzgS/zyMLeqDp9ub1bHulBNC0Cp92yjU7oFG9iNDPhVMwxToSvywwY1NQJpHn5B8JxA/uPeb5JhE= belder@BelderR14"
  public_key = local.ssh_public_key
}

# VPC
data "aws_vpc" "default" {
  default = true
}

# SECURITY GROUP
resource "aws_security_group" "allow_all" {
  name        = "${var.cluster_prefix}-WebDMZ_TRFRM"
  description = "Allow All inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${var.cluster_prefix}-WebDMZ_TRFRM"
  }
}


# data "aws_ami" "ubuntu" {
#   most_recent = true

#   filter {
#     name   = "name"
#     # values = ["Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type"]
#     values = ["ami-026b57f3c383c2eec"]
#   }

#   # filter {
#   #   name   = "virtualization-type"
#   #   values = ["hvm"]
#   # }

#   owners = ["amazon"] # Canonical
# }
