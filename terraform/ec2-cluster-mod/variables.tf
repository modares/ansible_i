variable "cluster_prefix" {
  type        = string
  description = "prefix for ec2 cluster"
}

variable "node_count" {
  type        = number
  description = "count of ec2 instances"
}

variable "ssh_public_key" {
  type        = string
  description = "cluster ssh_key"
}

variable "role_tag" {
  type        = string
  description = "role for the purpose of dynamic inventory"
}

variable "keep_instance_ids" {
  type        = list
  description = "list of instances to keep"
}
