variable "ssh_public_key" {
  type        = string
  description = "public ssh key for the ec2 isntance launching template"
}

variable "ansible_role" {
  type        = string
  description = "ansible role to assign to an instance"
}

variable "launch_template_name" {
  type        = string
  description = "name of the launching template"
}

variable "role_tag" {
  type        = string
  description = "role for the purpose of dynamic inventory"
}