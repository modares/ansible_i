locals {
  key_pair_name  = "${var.launch_template_name}-key"
  ec2_name       = "${var.launch_template_name}"
  ssh_public_key = var.ssh_public_key
  role_tag       = var.role_tag
}

# PUBLIC_KEY
resource "aws_key_pair" "ec2_cluster_key" {
  key_name = local.key_pair_name
  # public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCywzlyebnuQ8M6eGSkJfQeU+bX+QyykUH9ku69bhvnIU4+eKvVq/SOWl8x0xhe961ge4Z0XbRyWSFvM5TdQr/slRaiEaTubar4Wb7B5HljSsefJrp3PBiuwaVq5SOWUwnvqzzoS5uBKr13hdbpD9EQOGQ8FcVtxFFDbVFhPg5klg7IebIeGoltjcSFIsYv+OcNL1IwLAv1W6sBRdh3F4eKft57dYtSD3USTjQzH8Q2fTZ2n1oOz1i6Pk7reNyvb5R5xXiTtjtOhd7pdELtP9MN1pnZmNSbdyaL/lBFBhI5L7bfpFB54T28bXaHlKqFOM3zptpEhH/DyoVJbRNhAVpLpLg0HVKtyEI+OC52wWd/8Div5q1xFVkWcORUot+4VVc8u2gx/vELYXM1xerIcDqUayUyLxn681+/S3n7l+HylXiWwhwsFlUXzgS/zyMLeqDp9ub1bHulBNC0Cp92yjU7oFG9iNDPhVMwxToSvywwY1NQJpHn5B8JxA/uPeb5JhE= belder@BelderR14"
  public_key = local.ssh_public_key
}

resource "aws_launch_template" "iics_sa" {
  name = var.launch_template_name

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = 20
    }
  }

  #   cpu_options {
  #     core_count       = 4
  #     threads_per_core = 2
  #   }

  #   credit_specification {
  #     cpu_credits = "standard"
  #   }

  disable_api_stop        = false
  disable_api_termination = false

  ebs_optimized = false

  #   iam_instance_profile {
  #     name = "test"
  #   }

  #   image_id = "ami-test"
  image_id = "ami-0aa7d40eeae50c9a9"
  # image_id = "ami-08e637cea2f053dfa"
  #  amazon linux ami-0aa7d40eeae50c9a9
  #   ami                         = "ami-08c40ec9ead489470" # Ubutnu
  #   ami                         = "ami-08e637cea2f053dfa" # Redhat 9
  #   ami                         = "ami-045393c081cabeb1f" # Redhat 7.9

  #   instance_initiated_shutdown_behavior = "terminate"

  #   instance_market_options {
  #     market_type = "spot"
  #   }

  instance_type = "t2.micro"

  #   kernel_id = "test"

  key_name = local.key_pair_name

  #   license_specification {
  #     license_configuration_arn = "arn:aws:license-manager:eu-west-1:123456789012:license-configuration:lic-0123456789abcdef0123456789abcdef"
  #   }

  metadata_options {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
    instance_metadata_tags      = "enabled"
  }

  monitoring {
    enabled = true
  }

  # network_interfaces {
  #   associate_public_ip_address = true
  # }

  placement {
    availability_zone = "us-east-1a"
  }

  #   ram_disk_id = "test"

  #   vpc_security_group_ids = ["sg-12345678"]
  ######
  vpc_security_group_ids = [aws_security_group.allow_all.id]

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = local.ec2_name
      Role = local.role_tag
    }
  }

  #   user_data = filebase64("${path.module}/example.sh")
}

data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "allow_all" {
  name        = "autoscaling-WebDMZ_TRFRM"
  description = "Allow All inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "autoscaling-WebDMZ_TRFRM"
  }
}