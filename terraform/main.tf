terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      # version = "~> 3.27"
      # version = "~> 4"
    }
  }
}

locals {
  ec2_template_name = "iics_dev"
}

provider "aws" {
  region  = "us-east-1"
  profile = "acloud"
}

# module "ec2_launch_template" {
#   source               = "./launch_template"
#   ssh_public_key       = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFMbCuPs2yME34Kg8iFww9k/EiYzOIHYp3vW4akt/ixEPYifJHE5k9ZvsqrOk3eseZmlINfWOn8ONieiGS2Uo41B2GKWPsUbvs5rv8QzMqHbfsprQeedQYSdEgkl1upYLm6iPrPVyS4x8pwOvUNXyBpIngUGcVqp9TcDDawNmJL9/ips8tkwHvltlqg6/O+5WLoN0/hH9uSUMcl9N59J5ytYmBORz/2LeSDdWfriJDtUPnitv8bozyCek6UtGfebWec3Mf+O7CdZrAGDYJHFNDX5pEL0Z39Jm4pA1ZsuvOjlhOfa3634cZjXJNDCJFfCYud7tCL6+dEbHzO3BnIcJjX0lvWJpWuPyz7XuLR8ijhHRCQ3fVjs2U/M5pOZDRHWhoV+flvU2GtTownSsoBkmfQk5rPFRP+kxIyixJ6NhUILG1Zk2doeblcAePAl4fMiM7pJgu0gxd4/dJUzIWzHyG6LeqdHldXQfY2TQ7/+YGXi6/bdOuBvCfg7g+WvOGEgU= belder@BelderR14"
#   ansible_role         = "iics_cdi"
#   launch_template_name = local.ec2_template_name
#   role_tag             = "iics_cdi"
# }

# launch_template 
data "aws_vpc" "default" {
  default = true
}

resource "aws_security_group" "allow_all" {
  name        = "${local.ec2_template_name}-WebDMZ_TRFRM"
  description = "Allow All inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "TLS from VPC"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "${local.ec2_template_name}-WebDMZ_TRFRM"
  }
}

# resource "aws_autoscaling_group" "dev_iics_autoscaling" {
#   name               = "jibud_scaler"
#   availability_zones = ["us-east-1a"]
#   desired_capacity   = 0
#   max_size           = 3
#   min_size           = 1

#   launch_template {
#     id      = module.ec2_launch_template.template_id
#     version = "$Latest"
#   }

#   # tags = [ {
#   #   "Name" = "jibud"
#   # } ]
# }

# resource "aws_autoscaling_policy" "example" {
#   autoscaling_group_name = "jibud_scaler"
#   name                   = "foo"
#   policy_type            = "TargetTrackingScaling"

#   target_tracking_configuration {
#     predefined_metric_specification {
#       predefined_metric_type = "ASGAverageCPUUtilization"
#     }
#     target_value = 60
#   }
# }


module "cluster_hadi" {
  source         = "./ec2-cluster-mod"
  ssh_public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDFMbCuPs2yME34Kg8iFww9k/EiYzOIHYp3vW4akt/ixEPYifJHE5k9ZvsqrOk3eseZmlINfWOn8ONieiGS2Uo41B2GKWPsUbvs5rv8QzMqHbfsprQeedQYSdEgkl1upYLm6iPrPVyS4x8pwOvUNXyBpIngUGcVqp9TcDDawNmJL9/ips8tkwHvltlqg6/O+5WLoN0/hH9uSUMcl9N59J5ytYmBORz/2LeSDdWfriJDtUPnitv8bozyCek6UtGfebWec3Mf+O7CdZrAGDYJHFNDX5pEL0Z39Jm4pA1ZsuvOjlhOfa3634cZjXJNDCJFfCYud7tCL6+dEbHzO3BnIcJjX0lvWJpWuPyz7XuLR8ijhHRCQ3fVjs2U/M5pOZDRHWhoV+flvU2GtTownSsoBkmfQk5rPFRP+kxIyixJ6NhUILG1Zk2doeblcAePAl4fMiM7pJgu0gxd4/dJUzIWzHyG6LeqdHldXQfY2TQ7/+YGXi6/bdOuBvCfg7g+WvOGEgU= belder@BelderR14"
  cluster_prefix = "awx"
  node_count     = 3
  role_tag       = "AWX_host"
  keep_instance_ids = ["instance_id_1", "instance_id_2"]
}